package service.repositories.imp;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import service.repositories.BookRepository;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import domain.Book;

public class BookRepoTest {
	
	private static RepositoryCatalogImp catalog;
	private static BookRepository testedRepo;
	
	private static Book book;
	
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RepositoryCatalog catalog2 = new RepositoryCatalogImp();
		testedRepo = catalog2.getBookRepo();
		
		//List<Book> list = generateTestBooks();
		
		book = new Book();
		book.setTitle("DA");
		
		testedRepo.add(book);
		//book = new BookFactory(catalog).createBook(1, "Przeminelo z wiadrem",
		//		"Dawid", "Kwidzinski", "Kryminal");
		//book = new Book();
		//book.setId(1);
		//book.setTitle("Przeminelo z wiadrem");
		//book.setAuthorName("Dawid");
		//book.setAuthorSurname("Kwidzinski");
		
	}

	private static List<Book> generateTestBooks() {
		List<Book> createdBooks = new ArrayList<Book>();
		
		Book testBook1 = Mockito.mock(Book.class);
		
		when(testBook1.getId()).thenReturn(0);
		when(testBook1.getTitle()).thenReturn("Przeminelo z wiadrem");
		when(testBook1.getAuthorName()).thenReturn("Dawid");
		when(testBook1.getAuthorSurname()).thenReturn("Kwidzinski");
		
		Book testBook2 = Mockito.mock(Book.class);
		
		when(testBook2.getId()).thenReturn(0);
		when(testBook2.getTitle()).thenReturn("Java");
		when(testBook2.getAuthorName()).thenReturn("Panicz");
		when(testBook2.getAuthorSurname()).thenReturn("Wysocki");
		
		return createdBooks;
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAdd() {
		
		testedRepo.add2(book);
		
		Book bookFound = testedRepo.get(1);
		
		assertEquals(bookFound.getId(), book.getId());
		assertEquals(bookFound.getTitle(), book.getTitle());
		assertEquals(bookFound.getAuthorName(), book.getAuthorName());
		assertEquals(bookFound.getAuthorSurname(), book.getAuthorSurname());
	
	}
		
	
	@Test
	public void testSearchByTitle() {
		
		List<Book> foundBooks = testedRepo.searchByTitle("Przeminelo z wiadrem");
		
		Book bookFound = foundBooks.get(0);
		
		assertEquals(bookFound.getId(), book.getId());
		assertEquals(bookFound.getTitle(), book.getTitle());
		assertEquals(bookFound.getAuthorName(), book.getAuthorName());
		assertEquals(bookFound.getAuthorSurname(), book.getAuthorSurname());
		
	}

	@Test
	public void testSearchByAuthorName() {

		List<Book> foundBooks = testedRepo.searchByAuthorName("Dawid");
		
		Book bookFound = foundBooks.get(0);
		
		assertEquals(bookFound.getId(), book.getId());
		assertEquals(bookFound.getTitle(), book.getTitle());
		assertEquals(bookFound.getAuthorName(), book.getAuthorName());
		assertEquals(bookFound.getAuthorSurname(), book.getAuthorSurname());
		
	}

	@Test
	public void testSearchByAuthorSurname() {
		Book bookFound = testedRepo.searchByAuthorSurname("Kwidzinski").get(0);
		
		assertEquals(bookFound.getId(), book.getId());
		assertEquals(bookFound.getTitle(), book.getTitle());
		assertEquals(bookFound.getAuthorName(), book.getAuthorName());
		assertEquals(bookFound.getAuthorSurname(), book.getAuthorSurname());
		
	}

	//@Test
	public void testSearchByKind() {
		Book bookFound = testedRepo.searchByKind("Kryminal").get(0);
		
		assertEquals(bookFound.getId(), book.getId());
		assertEquals(bookFound.getTitle(), book.getTitle());
		assertEquals(bookFound.getAuthorName(), book.getAuthorName());
		assertEquals(bookFound.getAuthorSurname(), book.getAuthorSurname());
			
	}

	//@Test
	public void testSearchByTags() {
		List<String> tagsList = new ArrayList<String>();
		
		tagsList.add("Seba");
		
		List<Book> foundBooks = testedRepo.searchByTags(tagsList);
		
		assertNotNull(foundBooks);
		//assertSame(foundBooks.get(0), dummy.books.get(2));
		 
	}

}

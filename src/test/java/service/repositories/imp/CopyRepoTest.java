package service.repositories.imp;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Paradygmat.TimeConverter;
import service.ConnectionManager;
import service.repositories.CopyRepository;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import domain.Book;
import domain.Copy;
import domain.Copy.Status;
import factories.factoriesImp.BookFactory;
import factories.factoriesImp.CopyFactory;

public class CopyRepoTest {

	static RepositoryCatalog catalog;
	private static CopyRepository testedRepo;
	
	private static Connection connection;
	private static Statement statement;

	private static Book book;
	private static Copy copy;
	private static Copy copy1;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		String createTable = "CREATE TABLE COPY (id int GENERATED BY DEFAULT AS IDENTITY (START WITH 1), releasenumber int, releasedate bigint, bookid int,"
				+ "status varchar(30))";
		
		connection = ConnectionManager.getConnection();
		catalog = new RepositoryCatalogImp(connection);
		
		try{
			statement = connection.createStatement();
			
			boolean tableExists = false;
			
			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
			while (rs.next()){
				if ("Copy".equalsIgnoreCase(rs.getString("TABLE_NAME"))){
					tableExists = true;
					break;
				}
			}	
				if (!tableExists)
					statement.execute(createTable);
		} catch (SQLException e){
			e.printStackTrace();
		}
		
		testedRepo = catalog.getCopyRepo();
		
		book = new BookFactory(catalog).createBook(1, "Przeminelo z wiadrem",
				"Dawid", "Kwidzinski", "Kryminal");
		
		copy = new CopyFactory(catalog).createCopy(1, 1, TimeConverter.getDay("10/10/1990"), book);
		copy1 = new CopyFactory(catalog).createCopy(2, 1, TimeConverter.getDay("07/12/1970"), book);
		

		testedRepo.add(copy);
		testedRepo.add(copy);
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAdd() {
		Copy copyFound = testedRepo.get(1);
		
		assertEquals(copyFound.getId(), copy.getId());
		assertEquals(copyFound.getReleaseNumber(), copy.getReleaseNumber());
		assertEquals(copyFound.getReleaseDate(), copy.getReleaseDate());
		
	}

	@Test
	public void testFindByReleaseNumber() {
		List<Copy> foundBooks = testedRepo.searchByReleaseNumber(1);
		
		Copy copyFound = foundBooks.get(0);
		
		assertEquals(copyFound.getId(), copy.getId());
		assertEquals(copyFound.getReleaseNumber(), copy.getReleaseNumber());
		assertEquals(copyFound.getReleaseDate(), copy.getReleaseDate());
	}

	@Test
	public void testFindByReleaseDate() {
		List<Copy> foundBooks = testedRepo.searchByReleaseDate(TimeConverter.getDay("07/12/1970"));
		
		Copy copyFound = foundBooks.get(0);
		
		assertEquals(copyFound.getId(), copy.getId());
		assertEquals(copyFound.getReleaseNumber(), copy.getReleaseNumber());
		assertEquals(copyFound.getReleaseDate(), copy.getReleaseDate());
	
	}
	
	@Test
	public void testFindByStatus() {
		List<Copy> foundBooks = testedRepo.searchByStatus(Status.AVAILABLE);
		
		Copy copyFound = foundBooks.get(0);
		
		assertEquals(copyFound.getId(), copy.getId());
		assertEquals(copyFound.getReleaseNumber(), copy.getReleaseNumber());
		assertEquals(copyFound.getReleaseDate(), copy.getReleaseDate());
	}
}

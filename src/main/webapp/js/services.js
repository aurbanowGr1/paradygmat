angular
.module('Paradygmat')
	.factory('bookFactory', function($http) {
		var get = function(id) {
			return $http.get('http://localhost:8080/Paradygmat/rest/books/get/' + id);
		};
		var getAll = function() {
			return $http.get('http://localhost:8080/Paradygmat/rest/books/getAll');
		};
		var add = function(newItem) {
			return $http.post('http://localhost:8080/Paradygmat/rest/books/add', newItem);
		};
		var remove = function(id) {
			return $http.delete('http://localhost:8080/Paradygmat/rest/books/delete/' + id);
		};		
		var update = function(id, changedItem) {
			return $http.put('http://localhost:8080/Paradygmat/rest/books/update/' + id, changedItem);
		};
		return {
			get: get,
			getAll: getAll,
			add: add,
			remove: remove,
			update: update
		};
	});
angular
.module('Paradygmat')
	.factory('copyFactory', function($http) {
		var get = function(id) {
			return $http.get('http://localhost:8080/Paradygmat/rest/copies/get/' + id);
		};
		var getAll = function() {
			return $http.get('http://localhost:8080/Paradygmat/rest/copies/getAll');
		};
		var add = function(newItem) {
			return $http.post('http://localhost:8080/Paradygmat/rest/copies/add', newItem);
		};
		var remove = function(id) {
			return $http.delete('http://localhost:8080/Paradygmat/rest/copies/delete/' + id);
		};		
		var update = function(id, changedItem) {
			return $http.put('http://localhost:8080/Paradygmat/rest/copies/update/' + id, changedItem);
		};
		return {
			get: get,
			getAll: getAll,
			add: add,
			remove: remove,
			update: update
		};
});
angular
.module('Paradygmat')
	.factory('readerFactory', function($http) {
		var get = function(id) {
			return $http.get('http://localhost:8080/Paradygmat/rest/readers/get/' + id);
		};
		var getAll = function() {
			return $http.get('http://localhost:8080/Paradygmat/rest/readers/getAll');
		};
		var add = function(newItem) {
			return $http.post('http://localhost:8080/Paradygmat/rest/readers/add', newItem);
		};
		var remove = function(id) {
			return $http.delete('http://localhost:8080/Paradygmat/rest/readers/delete/' + id);
		};		
		var update = function(id, changedItem) {
			return $http.put('http://localhost:8080/Paradygmat/rest/readers/update/' + id, changedItem);
		};
		return {
			get: get,
			getAll: getAll,
			add: add,
			remove: remove,
			update: update
		};
});
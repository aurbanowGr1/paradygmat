angular
	.module('Paradygmat', ['ui.router', 'ui.bootstrap'])
	.config(function ($stateProvider, $urlRouterProvider) {	
		$urlRouterProvider.otherwise('/');
		
		$stateProvider
			.state('main', {
				url: '/',
				templateUrl: 'views/main.html',
				controller: 'MainCtrl'
				})
			.state('books', {
				url: '/books',
				templateUrl: 'views/books.html',
				controller: 'BooksCtrl'
				})
			.state('books.bookChanger', {
				url: '/{id}',
				templateUrl: 'views/bookChanger.html',
				controller: 'BookChangerCtrl'
				})
			.state('readers', {
				url: '/readers',
				templateUrl: 'views/readers.html',
				controller: 'ReadersCtrl'
				})
			.state('readers.readerChanger', {
				url: '/{id}',
				templateUrl: 'views/readerChanger.html',
				controller: 'ReaderChangerCtrl'
				})
			.state('copies', {
				url: '/copies',
				templateUrl: 'views/copies.html',
				controller: 'CopiesCtrl'
				})
			.state('copies.copyChanger', {
				url: '/{id}',
				templateUrl: 'views/copyChanger.html',
				controller: 'CopyChangerCtrl'
				})

	});	
toastr.options = {
  "closeButton": false,
  "debug": false,
  "progressBar": false,
  "positionClass": "toast-bottom-center",
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "3000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};
angular
	.module('Paradygmat')
	.controller('MainCtrl', function($scope) {
	
	});
angular
	.module('Paradygmat')
	.controller('BooksCtrl', function($scope, $http, bookFactory) {
		$scope.showData = function() {
			bookFactory.getAll()
			.success(function(response) {
					$scope.books = response;
				});
		};
		$scope.showData();
		$scope.add = function() {
			bookFactory.add($scope.newBook)
			.success(function(response) {
				toastr.success("OK");
				$scope.showData();
				$scope.newBook = {};
			});
		};
		$scope.delete = function(id) {
			bookFactory.remove(id)
			.success(function(response) {
					toastr.success("DELETED");
					$scope.showData();
				});
		};
	});
angular
	.module('Paradygmat')
	.controller('BookChangerCtrl', function($scope, bookFactory, $stateParams, $location) {
		bookFactory.get($stateParams.id)
			.success(function(response) {
					$scope.bookToChange = response;
			}); 
		$scope.change = function() {
			bookFactory.update($stateParams.id, $scope.bookToChange)
				.success(function(response) {
					toastr.success("UPDATED");
					$scope.$parent.books.forEach( function(book) {
						if (book.id === $scope.bookToChange.id) {
							book.title = $scope.bookToChange.title;
							book.authorName = $scope.bookToChange.authorName;
							book.authorSurname = $scope.bookToChange.authorSurname;
						}	
					});
					$location.path("/books");
					
				});
		};

	});
angular
.module('Paradygmat')
	.controller('CopiesCtrl', function($scope, $http, copyFactory) {
		$scope.showData = function() {
			copyFactory.getAll()
			.success(function(response) {
					$scope.copies = response;
				});
		};
		$scope.showData();
		$scope.add = function() {
			copyFactory.add($scope.newCopy)
			.success(function(response) {
				toastr.success("OK");
				$scope.showData();
				$scope.newCopy = {};
			});
		};
		$scope.delete = function(id) {
			copyFactory.remove(id)
			.success(function(response) {
					toastr.success("DELETED");
					$scope.showData();
				});
		};
	});
	angular
	.module('Paradygmat')
	.controller('CopyChangerCtrl', function($scope, copyFactory, $stateParams, $location) {
		copyFactory.get($stateParams.id)
			.success(function(response) {
					$scope.copyToChange = response;
			}); 
		$scope.change = function() {
			copyFactory.update($stateParams.id, $scope.copyToChange)
				.success(function(response) {
					toastr.success("UPDATED");
					$scope.$parent.copies.forEach( function(copy) {
						if (copy.id === $scope.copyToChange.id) {
							copy.releaseNumber = $scope.copyToChange.releaseNumber;
							copy.releaseDate = $scope.copyToChange.releaseDate;
						}	
					});
					$location.path("/copies");
					
				});
		};
});
angular
	.module('Paradygmat')
		.controller('ReadersCtrl', function($scope, $http, readerFactory) {
			$scope.showData = function() {
				readerFactory.getAll()
				.success(function(response) {
						$scope.readers = response;
					});
			};
			$scope.showData();
			$scope.add = function() {
				readerFactory.add($scope.newReader)
				.success(function(response) {
					toastr.success("OK");
					$scope.showData();
					$scope.newReader = {};
				});
			};
			$scope.delete = function(id) {
				readerFactory.remove(id)
				.success(function(response) {
						toastr.success("DELETED");
						$scope.showData();
					});
			};
		});
angular
	.module('Paradygmat')
		.controller('ReaderChangerCtrl', function($scope, readerFactory, $stateParams, $location) {
			readerFactory.get($stateParams.id)
				.success(function(response) {
						$scope.readerToChange = response;
				}); 
			$scope.change = function() {
				readerFactory.update($stateParams.id, $scope.copyToChange)
					.success(function(response) {
						toastr.success("UPDATED");
						$scope.$parent.readers.forEach( function(reader) {
							if (reader.id === $scope.readerToChange.id) {
								reader.firstName = $scope.readerToChange.firstName;
								reader.surname = $scope.readerToChange.surname;
								reader.gender = $scope.readerToChange.gender;
								reader.pesel = $scope.readerToChange.pesel;
								reader.email = $scope.readerToChange.email;
								reader.phoneNumber = $scope.readerToChange.phoneNumber;
							}	
						});
						$location.path("/readers");
						
					});
			};
	});
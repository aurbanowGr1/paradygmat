package domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
	@NamedQuery(name="user.searchByLogin", query="SELECT u FROM User u WHERE u.login= :login"),
	@NamedQuery(name="user.seatchByReader", query="SELECT u FROM User u WHERE u.reader= :reader"),
	
})
public class User extends EntityBase {
	
	private String login;
	private String password;
	
	@OneToOne
	private Reader reader;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
}

package domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
	@NamedQuery(name="reader.searchByFirstName", query="SELECT r FROM Reader r WHERE r.firstName= :firstName"),
	@NamedQuery(name="reader.searchBySurname", query="SELECT r FROM Reader r WHERE r.surname= :surname"),
	//@NamedQuery(name="reader.searchByCity", query="SELECT r FROM Reader r WHERE r.city= :city"),
	@NamedQuery(name="reader.searchByGender", query="SELECT r FROM Reader r WHERE r.gender= :gender"),
	@NamedQuery(name="reader.searchByPesel", query="SELECT r FROM Reader r WHERE r.pesel= :pesel"),
	@NamedQuery(name="reader.searchByEmail", query="SELECT r FROM Reader r WHERE r.email= :email"),
	@NamedQuery(name="reader.searchByPhoneNumber", query="SELECT r FROM Reader r WHERE r.phoneNumber= :phoneNumber"),
	
})
public class Reader extends EntityBase {

	public enum Gender {
		MALE,
		FEMALE;
	}
	
	public Reader() {
		this.histories = new HashSet<History>();
		this.notifications = new HashSet<Notification>();
		this.requests = new HashSet<Request>();
	}
	
	private String firstName;
	private String surname;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	
	private String pesel;
	private String email;
	private String phoneNumber;
	
	@OneToOne(mappedBy = "Reader")//, cascade=CascadeType.ALL)
	private Address address;
	
	@OneToOne(mappedBy = "Reader")//, cascade=CascadeType.ALL)
	private User user;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Reader")//, cascade=CascadeType.ALL)
	private Set<Notification> notifications;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Reader")//, cascade=CascadeType.ALL)
	private Set<Request> requests;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Reader")//, cascade=CascadeType.ALL)
	private Set<History> histories;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}
	public Set<Request> getOrders() {
		return requests;
	}
	public void setOrders(Set<Request> orders) {
		this.requests = orders;
	}
	public Set<History> getHistories() {
		return histories;
	}
	public void setHistories(Set<History> histories) {
		this.histories = histories;
	}

}

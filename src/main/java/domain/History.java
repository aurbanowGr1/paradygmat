package domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "history.searchByBorrowDate", query = "SELECT h FROM History h WHERE h.borrowDate = :borrowDate"),
	@NamedQuery(name = "history.searchByReturnDate", query = "SELECT h FROM History h WHERE h.returnDate = :returnDate"),
	@NamedQuery(name = "history.searchByCopy", query = "SELECT h FROM History h WHERE h.copy = :copy"),
	@NamedQuery(name = "history.searchByReader", query = "SELECT h FROM History h WHERE h.reader = :reader"),
})
public class History extends EntityBase {
	private long borrowDate;
	private long returnDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Copy copy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Reader reader;
		
	public long getBorrowDate() {
		return borrowDate;
	}
	public void setBorrowDate(long borrowDate) {
		this.borrowDate = borrowDate;
	}
	public long getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(long returnDate) {
		this.returnDate = returnDate;
	}
	public Copy getCopy() {
		return copy;
	}
	public void setCopy(Copy copy) {
		this.copy = copy;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}	
}

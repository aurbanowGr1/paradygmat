package domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries({
	@NamedQuery(name = "copy.searchByReleaseNumber", query = "SELECT c FROM Copy c WHERE c.releaseNumber = :releaseNumber"),
	@NamedQuery(name = "copy.searchByStatus", query = "SELECT c FROM Copy c WHERE c.status = :status"),
	@NamedQuery(name = "copy.searchByDate", query = "SELECT c FROM Copy c WHERE c.releaseDate = :releaseDate"),
})
public class Copy extends EntityBase {
	public enum Status {
		ORDERED, 
		BORROWED, 
		AVAILABLE,
		OVERDUE,
		UNDEFINED;
	}
	
	public Copy() {
		
		this.notifications = new HashSet<Notification>();
		this.histories = new HashSet<History>();
	}
	
	private int releaseNumber;
	private long releaseDate;
	
	@ManyToOne
	private Book book;
	
	@Enumerated(EnumType.STRING)
	private Status status;
	
	@OneToMany(mappedBy="Copy", cascade=CascadeType.ALL)
	private Set<Notification> notifications;
	@OneToMany(mappedBy="Copy", cascade=CascadeType.ALL)
	private Set<History> histories;
	
	public int getReleaseNumber() {
		return releaseNumber;
	}
	public void setReleaseNumber(int releaseNumber) {
		this.releaseNumber = releaseNumber;
	}
	public long getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public Set<Notification> getNotifications() {
		return notifications;
	}
	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}
	public Set<History> getHistories() {
		return histories;
	}
	public void setHistories(Set<History> histories) {
		this.histories = histories;
	}
}

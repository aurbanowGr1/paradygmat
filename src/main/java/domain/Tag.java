package domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name="tag.searchByBook", query="SELECT t FROM Tag t WHERE t.book= :book"),
	@NamedQuery(name="tag.searchByKeyword", query="SELECT t FROM Tag t WHERE t.keyword= :keyword"),
	
})
public class Tag extends EntityBase {
	
	@ManyToOne(fetch = FetchType.LAZY)//, cascade=CascadeType.ALL)
	private Book book;

	private String keyword;
	
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}

}

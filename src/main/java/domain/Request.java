package domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name="request.searchByRequestDate", query="SELECT r FROM Request r WHERE r.requestDate= :requestDate"),
	@NamedQuery(name="request.searchByExecutionDate", query="SELECT r FROM Request r WHERE r.executionDate= :executionDate"),
	@NamedQuery(name="request.searchByBook", query="SELECT r FROM Request r WHERE r.book= :book"),
	@NamedQuery(name="request.searchByReader", query="SELECT r FROM Request r WHERE r.reader= :reader"),
	
})
public class Request extends EntityBase {
	
	private long requestDate;
	private long executionDate;
	
	@ManyToOne
	private Book book;
	
	@ManyToOne
	private Reader reader;
	
	public long getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(long orderDate) {
		this.requestDate = orderDate;
	}
	public long getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(long executionDate) {
		this.executionDate = executionDate;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
}

package domain;

public enum State {
	NEW,
	DELETED,
	ALTERED,
	UNDEFINED
}

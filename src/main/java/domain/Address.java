package domain;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Address extends EntityBase {
	
	private String city;
	private String postalCode;
	private String street;
	private String houseNumber;
	private String apartmentNumber;
	
	@OneToOne
	private Reader reader;
	
	public String getCity() {
		return city;
	}	
	public void setCity(String city) {
		this.city = city;
	}	
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}	
	public String getStreet() {
		return street;
	}	
	public void setStreet(String street) {
		this.street = street;
	}	
	public String getHouseNumber() {
		return houseNumber;
	}	
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public String getApartmentNumber() {
		return apartmentNumber;
	}
	public void setApartmentNumber(String apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}
}

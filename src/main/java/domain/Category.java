package domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.CascadeType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@NamedQueries({
	@NamedQuery(name = "category.searchByKeyword", query = "SELECT c FROM Category c WHERE c.keyword = :keyword"),
	@NamedQuery(name = "category.searchByBook", query = "SELECT c FROM Category c WHERE c.book = :book"),
})
public class Category extends EntityBase {

	
	private String keyword;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Category parent;
	
	@OneToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Category child;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private Book book;

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public Category getParent() {
		return parent;
	}
	public void setParent(Category parent) {
		this.parent = parent;
	}
	public Category getChild() {
		return child;
	}
	public void setChild(Category child) {
		this.child = child;
	}


}

package domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.NamedQuery;
import javax.persistence.NamedQueries;;

@Entity
@NamedQueries({
	@NamedQuery(name="book.searchByTitle", query="SELECT b FROM Book b WHERE b.title= :title"),
	@NamedQuery(name="book.searchByAuthorName", query="SELECT b FROM Book b WHERE b.authorName= :authorName"),
	@NamedQuery(name="book.searchByAuthorSurname", query="SELECT b FROM Book b WHERE b.authorSurname= :authorSurname"),
	//kategorie do zrobienia
	//@NamedQuery(name="book.searchByKind", query="SELECT b FROM Book b WHERE b.kind= :kind"),
	//tagi do zrobienia
	//@NamedQuery(name="book.searchByTitle", query="SELECT b FROM BOOK b WHERE b.title= :title"),
	
})
public class Book extends EntityBase {

	public Book() {
		this.tags = new HashSet<Tag>();
		this.copies = new HashSet<Copy>();
		this.requests = new HashSet<Request>();
	}
	private String title;
	private String authorName;
	private String authorSurname;
	
	@OneToMany(mappedBy="Book")
	private Set<Category> categories;

	@OneToMany(mappedBy="Book")
	private Set<Tag> tags;

	@OneToMany(mappedBy="Book", cascade=CascadeType.ALL)
	private Set<Copy> copies;
	
	@OneToMany(mappedBy="Book", cascade=CascadeType.ALL)
	private Set<Request> requests;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public String getAuthorSurname() {
		return authorSurname;
	}
	public void setAuthorSurname(String authorSurname) {
		this.authorSurname = authorSurname;
	}
	public Set<Category> getCategories() {
		return categories;
	}
	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}
	public Set<Tag> getTags() {
		return tags;
	}
	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}
	public Set<Copy> getCopies() {
		return copies;
	}
	public void setCopies(Set<Copy> copies) {
		this.copies = copies;
	}
	public Set<Request> getOrders() {
		return requests;
	}
	public void setOrders(Set<Request> orders) {
		this.requests = orders;
	}
}

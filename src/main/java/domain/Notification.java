package domain;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "notification.searchByReader", query = "SELECT n FROM Notification n WHERE n.reader = :reader"),
	@NamedQuery(name = "notification.searchByCopy", query = "SELECT n FROM Notification n WHERE n.copy = :copy"),
	@NamedQuery(name = "notification.searchByDate", query = "SELECT n FROM Notification n WHERE n.date = :date"),
})
public class Notification extends EntityBase {
	
	private String title;
	private String message;
	private long date;
	
	@ManyToOne
	private Reader reader;
	
	@ManyToOne
	private Copy copy;
	
		
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public Reader getReader() {
		return reader;
	}
	public void setReader(Reader reader) {
		this.reader = reader;
	}
	public Copy getCopy() {
		return copy;
	}
	public void setCopy(Copy copy) {
		this.copy = copy;
	}

}

package services;

import javax.inject.Inject;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import domain.Address;


@WebService
public class TestService {

	@Inject
	private EntityManager em;
	
	@Transactional
	public int test()
	{
		try{
			Address address = em.find(Address.class, 51);
			address.setCity("test");
		
			
		}catch(Exception ex)
		{
			return -1;
		}
		return 1;
	}
}
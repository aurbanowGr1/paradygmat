package services;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EMProducer {

	@PersistenceContext(name="Paradygmat")
	@Produces
	private EntityManager em;
}
package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.Tag;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import services.dto.TagSummaryDto;

@Path("tags")
public class TagRestService {

	@Inject
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveTag(TagSummaryDto tag){
		Tag t = new Tag();
		
		t.setKeyword(tag.getKeyword());
		
		catalog.getTagRepo().add(t);
		
		return "OK";
	}
}

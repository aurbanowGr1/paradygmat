package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import domain.Book;
import domain.Reader;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import services.dto.BookSummaryDto;
import services.dto.ReaderSummaryDto;

@Path("readers")
public class ReaderRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String addReader(ReaderSummaryDto reader){
		Reader b = new Reader();
		
		b.setFirstName(reader.getFirstName());
		b.setSurname(reader.getSurname());
		b.setEmail(reader.getEmail());
		b.setPesel(reader.getPesel());
		
		catalog.getReaderRepo().add(b);
		
		return "OK";
	}
		
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<ReaderSummaryDto> getReader(){
		List<ReaderSummaryDto> result = new ArrayList<ReaderSummaryDto>();
		
		List<Reader> allReaders = catalog.getReaderRepo().getAll();
		for (Reader b : allReaders) {
			ReaderSummaryDto newReaderDto = new ReaderSummaryDto();
			newReaderDto.setId(b.getId());
			newReaderDto.setFirstName(b.getFirstName());
			newReaderDto.setSurname(b.getSurname());
			newReaderDto.setEmail(b.getEmail());
			newReaderDto.setPesel(b.getPesel());
			newReaderDto.setPhoneNumber(b.getPhoneNumber());
			
			result.add(newReaderDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public ReaderSummaryDto getBook(@PathParam("id") int id){

		Reader foundReader = catalog.getReaderRepo().get(id);
		
		ReaderSummaryDto newReaderDto = new ReaderSummaryDto();
		
		newReaderDto.setId(foundReader.getId());
		newReaderDto.setEmail(foundReader.getEmail());
		newReaderDto.setFirstName(foundReader.getFirstName());
		newReaderDto.setSurname(foundReader.getSurname());
		newReaderDto.setPesel(foundReader.getPesel());
		newReaderDto.setPhoneNumber(foundReader.getPhoneNumber());
		
		return newReaderDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteReader(@PathParam("id") int id) {
	
		Reader foundReader = catalog.getReaderRepo().get(id);
		catalog.getReaderRepo().delete(foundReader);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateReader(ReaderSummaryDto reader, @PathParam("id") int id){
		Reader foundReader = catalog.getReaderRepo().get(id);
		
		foundReader.setFirstName(foundReader.getFirstName());
		foundReader.setSurname(foundReader.getSurname());
		foundReader.setEmail(foundReader.getEmail());
		foundReader.setPesel(foundReader.getPesel());
		foundReader.setPhoneNumber(foundReader.getPhoneNumber());
		
		return "UPDATED";
	}
}

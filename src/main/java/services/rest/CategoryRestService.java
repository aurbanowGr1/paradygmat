package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ejb.EJB;

import domain.Category;
import service.repositories.RepositoryCatalog;
import services.dto.CategorySummaryDto;

@Path("categories")
public class CategoryRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveCategory(CategorySummaryDto category){
		Category c = new Category();
		
		c.setKeyword(category.getKeyword());
		
		catalog.getCategoryRepo().add(c);
		
		return "OK";
	}
	
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<CategorySummaryDto> getCategories(){
		List<CategorySummaryDto> result = new ArrayList<CategorySummaryDto>();
		
		List<Category> allCategories = catalog.getCategoryRepo().getAll();
		for (Category c : allCategories) {
			CategorySummaryDto newCategoryDto = new CategorySummaryDto();
			newCategoryDto.setId(c.getId());
			newCategoryDto.setKeyword(c.getKeyword());
			result.add(newCategoryDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public CategorySummaryDto getCategory(@PathParam("id") int id){
		Category foundCategory = catalog.getCategoryRepo().get(id);
		
		CategorySummaryDto newCategoryDto = new CategorySummaryDto();
		newCategoryDto.setId(foundCategory.getId());
		newCategoryDto.setKeyword(foundCategory.getKeyword());
		
		return newCategoryDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteCategory(@PathParam("id") int id) {
	
		Category foundCategory = catalog.getCategoryRepo().get(id);
		catalog.getCategoryRepo().delete(foundCategory);
		
		return "DELETED";
	}
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateCategory(CategorySummaryDto category, @PathParam("id") int id) {
		Category foundCategory = catalog.getCategoryRepo().get(id);
		
		foundCategory.setId(category.getId());
		foundCategory.setKeyword(category.getKeyword());
		
		return "UPDATED";
	}
	
}

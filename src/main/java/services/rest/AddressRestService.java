package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ejb.EJB;

import service.repositories.RepositoryCatalog;
import services.dto.AddressSummaryDto;
import domain.Address;

@Path("addresses")
public class AddressRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveAddress(AddressSummaryDto address){
		Address a = new Address();
		
		a.setCity(address.getCity());
		a.setPostalCode(address.getPostalCode());
		a.setStreet(address.getStreet());
		a.setHouseNumber(address.getHouseNumber());
		a.setApartmentNumber(address.getApartmentNumber());
		
		catalog.getAddressRepo().add(a);
		
		return "OK";
	}

	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<AddressSummaryDto> getAddresses(){
		List<AddressSummaryDto> result = new ArrayList<AddressSummaryDto>();
		
		List<Address> allAddresses = catalog.getAddressRepo().getAll();
		for (Address a : allAddresses) {
			AddressSummaryDto newAddressDto = new AddressSummaryDto();
			newAddressDto.setId(a.getId());
			newAddressDto.setCity(a.getCity());
			newAddressDto.setPostalCode(a.getPostalCode());
			newAddressDto.setStreet(a.getStreet());
			newAddressDto.setHouseNumber(a.getHouseNumber());
			newAddressDto.setApartmentNumber(a.getApartmentNumber());
			
			result.add(newAddressDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public AddressSummaryDto getAddress(@PathParam("id") int id){

		Address foundAddress = catalog.getAddressRepo().get(id);
		
		AddressSummaryDto newAddressDto = new AddressSummaryDto();
		newAddressDto.setId(foundAddress.getId());
		newAddressDto.setCity(foundAddress.getCity());
		newAddressDto.setPostalCode(foundAddress.getPostalCode());
		newAddressDto.setStreet(foundAddress.getStreet());
		newAddressDto.setHouseNumber(foundAddress.getHouseNumber());
		newAddressDto.setApartmentNumber(foundAddress.getApartmentNumber());
		
		return newAddressDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteAddress(@PathParam("id") int id) {
	
		Address foundAddress = catalog.getAddressRepo().get(id);
		catalog.getAddressRepo().delete(foundAddress);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateAddress(AddressSummaryDto address, @PathParam("id") int id){

		Address foundAddress = catalog.getAddressRepo().get(id);
		
		foundAddress.setCity(address.getCity());
		foundAddress.setPostalCode(address.getPostalCode());
		foundAddress.setStreet(address.getStreet());
		foundAddress.setHouseNumber(address.getHouseNumber());
		foundAddress.setApartmentNumber(address.getApartmentNumber());
		
		return "UPDATED";
	}

}

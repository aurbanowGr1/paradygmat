package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ejb.EJB;

import domain.Book;
import service.repositories.RepositoryCatalog;
import services.dto.BookSummaryDto;

@Path("books")
public class BookRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String addBook(BookSummaryDto book){
		Book b = new Book();
		
		b.setTitle(book.getTitle());
		b.setAuthorName(book.getAuthorName());
		b.setAuthorSurname(book.getAuthorSurname());
		
		catalog.getBookRepo().add(b);
		
		return "OK";
	}
		
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<BookSummaryDto> getBooks(){
		List<BookSummaryDto> result = new ArrayList<BookSummaryDto>();
		
		List<Book> allBooks = catalog.getBookRepo().getAll();
		for (Book b : allBooks) {
			BookSummaryDto newBookDto = new BookSummaryDto();
			newBookDto.setId(b.getId());
			newBookDto.setAuthorName(b.getAuthorName());
			newBookDto.setAuthorSurname(b.getAuthorSurname());
			newBookDto.setTitle(b.getTitle());
			result.add(newBookDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public BookSummaryDto getBook(@PathParam("id") int id){

		Book foundBook = catalog.getBookRepo().get(id);
		
		BookSummaryDto newBookDto = new BookSummaryDto();
		newBookDto.setId(foundBook.getId());
		newBookDto.setAuthorName(foundBook.getAuthorName());
		newBookDto.setAuthorSurname(foundBook.getAuthorSurname());
		newBookDto.setTitle(foundBook.getTitle());
		
		return newBookDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteBook(@PathParam("id") int id) {
	
		Book foundBook = catalog.getBookRepo().get(id);
		catalog.getBookRepo().delete(foundBook);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateBook(BookSummaryDto book, @PathParam("id") int id){

		Book foundBook = catalog.getBookRepo().get(id);
		
		foundBook.setAuthorName(book.getAuthorName());
		foundBook.setAuthorSurname(book.getAuthorSurname());
		foundBook.setTitle(book.getTitle());
		
		return "UPDATED";
	}
	
}

package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import domain.Notification;
import service.repositories.RepositoryCatalog;
import services.dto.NotificationSummaryDto;

@Path("notifications")
public class NotificationRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveNotification(NotificationSummaryDto notification){
		Notification n = new Notification();
		
		n.setTitle(notification.getTitle());
		n.setMessage(notification.getMessage());
		n.setDate(notification.getDate());
		
		catalog.getNotificationRepo().add(n);
		
		return "OK";
	}
	
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<NotificationSummaryDto> getNotifications(){
		List<NotificationSummaryDto> result = new ArrayList<NotificationSummaryDto>();
		
		List<Notification> allNotifications = catalog.getNotificationRepo().getAll();
		for (Notification n : allNotifications) {
			NotificationSummaryDto newNotificationDto = new NotificationSummaryDto();
			newNotificationDto.setId(n.getId());
			newNotificationDto.setDate(n.getDate());
			newNotificationDto.setMessage(n.getMessage());
			newNotificationDto.setTitle(n.getTitle());
			
			result.add(newNotificationDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public NotificationSummaryDto getNotification(@PathParam("id") int id){

		Notification foundNotification = catalog.getNotificationRepo().get(id);
		
		NotificationSummaryDto newNotificationDto = new NotificationSummaryDto();
		newNotificationDto.setId(foundNotification.getId());
		newNotificationDto.setDate(foundNotification.getDate());
		newNotificationDto.setMessage(foundNotification.getMessage());
		newNotificationDto.setTitle(foundNotification.getTitle());
		
		return newNotificationDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteNotification(@PathParam("id") int id) {
	
		Notification foundNotification = catalog.getNotificationRepo().get(id);
		catalog.getNotificationRepo().delete(foundNotification);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateNotification(NotificationSummaryDto notification, @PathParam("id") int id){

		Notification foundNotification = catalog.getNotificationRepo().get(id);
		
		foundNotification.setDate(notification.getDate());
		foundNotification.setMessage(notification.getMessage());
		foundNotification.setTitle(notification.getTitle());
		
		return "UPDATED";
	}
}

package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ejb.EJB;

import domain.Copy;
import service.repositories.RepositoryCatalog;
import services.dto.CopySummaryDto;

@Path("copies")
public class CopyRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveCopy(CopySummaryDto copy){
		Copy c = new Copy();
		
		c.setReleaseNumber(copy.getReleaseNumber());
		c.setReleaseDate(copy.getReleaseDate());
		
		catalog.getCopyRepo().add(c);
		
		return "OK";
	}
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<CopySummaryDto> getCopies(){
		List<CopySummaryDto> result = new ArrayList<CopySummaryDto>();
		
		List<Copy> allCopies = catalog.getCopyRepo().getAll();
		for (Copy c : allCopies) {
			CopySummaryDto newCopyDto = new CopySummaryDto();
			newCopyDto.setId(c.getId());
			newCopyDto.setReleaseNumber(c.getReleaseNumber());
			newCopyDto.setReleaseDate(c.getReleaseDate());
			result.add(newCopyDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public CopySummaryDto getCopy(@PathParam("id") int id){

		Copy foundCopy = catalog.getCopyRepo().get(id);
		
		CopySummaryDto newCopyDto = new CopySummaryDto();
		newCopyDto.setId(foundCopy.getId());
		newCopyDto.setReleaseNumber(foundCopy.getReleaseNumber());
		newCopyDto.setReleaseDate(foundCopy.getReleaseDate());
		
		return newCopyDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteCopy(@PathParam("id") int id) {
	
		Copy foundCopy = catalog.getCopyRepo().get(id);
		catalog.getCopyRepo().delete(foundCopy);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateCopy(CopySummaryDto copy, @PathParam("id") int id){

		Copy foundCopy = catalog.getCopyRepo().get(id);
		
		foundCopy.setReleaseNumber(copy.getReleaseNumber());
		foundCopy.setReleaseDate(copy.getReleaseDate());;
		
		return "UPDATED";
	}
	
}

package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ejb.EJB;

import domain.History;
import service.repositories.RepositoryCatalog;
import services.dto.HistorySummaryDto;

@Path("histories")
public class HistoryRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveHistory(HistorySummaryDto history){
		History h = new History();
		
		h.setBorrowDate(history.getBorrowDate());
		h.setReturnDate(history.getReturnDate());
		
		catalog.getHistoryRepo().add(h);
		
		return "OK";
	}
	
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<HistorySummaryDto> getBooks(){
		List<HistorySummaryDto> result = new ArrayList<HistorySummaryDto>();
		
		List<History> allHistories = catalog.getHistoryRepo().getAll();
		for (History h : allHistories) {
			HistorySummaryDto newHistoryDto = new HistorySummaryDto();
			newHistoryDto.setId(h.getId());
			newHistoryDto.setBorrowDate(h.getBorrowDate());
			newHistoryDto.setReturnDate(h.getReturnDate());
			result.add(newHistoryDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public HistorySummaryDto getHistory(@PathParam("id") int id){

		History foundHistory = catalog.getHistoryRepo().get(id);
		
		HistorySummaryDto newHistoryDto = new HistorySummaryDto();
		newHistoryDto.setId(foundHistory.getId());
		newHistoryDto.setBorrowDate(foundHistory.getBorrowDate());
		newHistoryDto.setReturnDate(foundHistory.getReturnDate());
		
		return newHistoryDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteHistory(@PathParam("id") int id) {
	
		History foundHistory = catalog.getHistoryRepo().get(id);
		catalog.getHistoryRepo().delete(foundHistory);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateBook(HistorySummaryDto history, @PathParam("id") int id){

		History foundHistory = catalog.getHistoryRepo().get(id);
		
		foundHistory.setId(history.getId());
		foundHistory.setBorrowDate(history.getBorrowDate());
		foundHistory.setReturnDate(history.getReturnDate());
		
		return "UPDATED";
	}
}

package services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import domain.Reader;
import domain.Request;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import services.dto.ReaderSummaryDto;
import services.dto.RequestSummaryDto;

@Path("requests")
public class RequestRestService {

	@EJB
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveRequest(RequestSummaryDto request){
		Request r = new Request();
		
		r.setRequestDate(request.getRequestDate());
		r.setExecutionDate(request.getExecutionDate());
		
		catalog.getRequestRepo().add(r);
		
		return "OK";
	}
		
	@GET
	@Produces("text/html")
	public String test(){
		return "Testowy response z REST serwisu";
	}
	
	@GET
	@Path("/getAll")
	@Produces("application/json")
	@Transactional
	public List<RequestSummaryDto> getRequests(){
		List<RequestSummaryDto> result = new ArrayList<RequestSummaryDto>();
		
		List<Request> allReaders = catalog.getRequestRepo().getAll();
		for (Request b : allReaders) {
			RequestSummaryDto newRequestDto = new RequestSummaryDto();
			
			newRequestDto.setId(b.getId());
			newRequestDto.setExecutionDate(b.getExecutionDate());
			newRequestDto.setRequestDate(b.getRequestDate());
			
			result.add(newRequestDto);
		}
		
		return result;
	}
	
	@GET
	@Path("/get/{id}")
	@Produces("application/json")
	@Transactional
	public RequestSummaryDto getRequest(@PathParam("id") int id){

		Request foundRequest = catalog.getRequestRepo().get(id);
		
		RequestSummaryDto newRequestDto = new RequestSummaryDto();
		
		newRequestDto.setId(foundRequest.getId());
		newRequestDto.setExecutionDate(foundRequest.getExecutionDate());
		newRequestDto.setRequestDate(foundRequest.getRequestDate());
		
		return newRequestDto;
	}
	
	@DELETE
	@Path("/delete/{id}")
	@Produces("application/json")
	@Transactional
	public String deleteRequest(@PathParam("id") int id) {
	
		Request foundRequest = catalog.getRequestRepo().get(id);
		catalog.getRequestRepo().delete(foundRequest);
		
		return "DELETED";
	}
	
	
	@PUT
	@Path("/update/{id}")
	@Produces("application/json")
	@Transactional
	public String updateRequest(RequestSummaryDto reader, @PathParam("id") int id){
		
		Request foundRequest = catalog.getRequestRepo().get(id);
		
		foundRequest.setId(foundRequest.getId());
		foundRequest.setExecutionDate(foundRequest.getExecutionDate());
		foundRequest.setRequestDate(foundRequest.getRequestDate());
		
		return "UPDATED";
	}
}

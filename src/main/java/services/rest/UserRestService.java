package services.rest;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import domain.User;
import service.repositories.RepositoryCatalog;
import service.repositories.imp.RepositoryCatalogImp;
import services.dto.UserSummaryDto;

@Path("users")
public class UserRestService {

	@Inject
	RepositoryCatalog catalog;
	
	@POST
	@Path("/add")
	@Consumes("application/json")
	@Transactional
	public String saveUsers(UserSummaryDto user){
		User u = new User();
		
		u.setLogin(user.getLogin());
		u.setPassword(user.getPassword());
		
		catalog.getUserRepo().add(u);
		
		return "OK";
	}
}

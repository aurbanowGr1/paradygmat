package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NotificationSummaryDto {

	private int id;
	private String title;
	private String message;
	private long date;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}

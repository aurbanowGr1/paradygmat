package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class CopySummaryDto {

	private int id;
	private int releaseNumber;
	private long releaseDate;
	
	public long getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getReleaseNumber() {
		return releaseNumber;
	}
	public void setReleaseNumber(int releaseNumber) {
		this.releaseNumber = releaseNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

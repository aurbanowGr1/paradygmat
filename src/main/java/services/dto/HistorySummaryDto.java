package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class HistorySummaryDto {

	private int id;
	private long borrowDate;
	private long returnDate;
	
	public long getBorrowDate() {
		return borrowDate;
	}
	public void setBorrowDate(long borrowDate) {
		this.borrowDate = borrowDate;
	}
	public long getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(long returnDate) {
		this.returnDate = returnDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

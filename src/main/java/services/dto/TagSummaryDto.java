package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TagSummaryDto {

	private int id;
	private String keyword;

	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

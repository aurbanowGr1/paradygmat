package services.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RequestSummaryDto {

	private int id;
	private long requestDate;
	private long executionDate;
	
	public long getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(long requestDate) {
		this.requestDate = requestDate;
	}
	public long getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(long executionDate) {
		this.executionDate = executionDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}

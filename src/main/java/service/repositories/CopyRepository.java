package service.repositories;

import java.util.List;

import domain.Copy;
import domain.Copy.Status;

public interface CopyRepository extends Repository<Copy>{

	List<Copy> searchByReleaseNumber(int number);
	List<Copy> searchByReleaseDate(long date);
	List<Copy> searchByStatus(Status status);
	
}

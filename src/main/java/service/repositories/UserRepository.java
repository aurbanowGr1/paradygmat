package service.repositories;

import domain.Reader;
import domain.User;

public interface UserRepository extends Repository<User> {

	User searchByLogin(String login);
	User searchByReader(Reader reader);
}

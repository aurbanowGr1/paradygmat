package service.repositories;

import java.util.List;

import domain.Copy;
import domain.Notification;
import domain.Reader;

public interface NotificationRepository extends Repository<Notification> {
	
	List<Notification> searchByReader(Reader reader);
	List<Notification> searchByCopy(Copy copy);
	List<Notification> searchByDate(long date);
}

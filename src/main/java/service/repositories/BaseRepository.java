package service.repositories;


import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.List;

import domain.EntityBase;

public abstract class BaseRepository<T extends EntityBase> implements Repository<T>{

	private EntityManager em;
	
	final Class<T> typeParameterClass;
	
	public BaseRepository(EntityManager em, Class<T> typeParameterClass) {
			this.em = em;
			this.typeParameterClass = typeParameterClass;
	}
	
	@Override
	public T get(int id) {
		return em.find(typeParameterClass, id);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll() {
		Query query = em.createQuery("SELECT e FROM " + getTableName() + " e");
	    return (List<T>) query.getResultList();
	}
	@Override
	public void delete(T entity) {
		em.remove(entity);
	}
	@Override
	public void update(T entity) {

	}
	@Override
	public void add(T entity) {
		em.persist(entity);
	}
	
	protected abstract String getTableName();

}

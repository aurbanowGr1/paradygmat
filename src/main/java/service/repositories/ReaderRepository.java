package service.repositories;

import java.util.List;

import domain.Reader;
import domain.Reader.Gender;

public interface ReaderRepository extends Repository<Reader> {

	List<Reader> searchByFirstName(String firstName);
	List<Reader> searchBySurname(String surname);
	List<Reader> searchByCity(String city);
	List<Reader> searchByGender(Gender gender);
	Reader searchByPesel(String pesel);
	Reader searchByEmail(String email);
	Reader searchByPhoneNumber(String phoneNumber);
	
}

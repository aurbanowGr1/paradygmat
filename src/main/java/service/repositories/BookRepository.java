package service.repositories;

import java.util.*;

import domain.Book;

public interface BookRepository extends Repository<Book>{
	
	List<Book> searchByTitle(String title); 
	List<Book> searchByAuthorName(String name);
	List<Book> searchByAuthorSurname(String surname);
	List<Book> searchByKind(String kind);
	List<Book> searchByTags(List<String> tags);
	
}

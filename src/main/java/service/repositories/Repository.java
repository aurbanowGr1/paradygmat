package service.repositories;

import java.util.List;

import domain.EntityBase;

public interface Repository<T extends EntityBase> {

		public T get(int id);
		public List<T> getAll();
		public void add(T entity);
		public void delete(T entity);
		public void update(T entity);

}

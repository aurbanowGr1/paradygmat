package service.repositories;

import java.util.List;

import domain.Book;
import domain.Category;

public interface CategoryRepository extends Repository<Category>{

	List<Category> searchByBook(Book book);
	List<Category> searchByKeyword(String keyword);
	
}

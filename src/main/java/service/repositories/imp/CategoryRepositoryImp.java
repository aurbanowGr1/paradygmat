package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.Book;
import domain.Category;
import service.repositories.BaseRepository;
import service.repositories.CategoryRepository;

public class CategoryRepositoryImp extends BaseRepository<Category> implements CategoryRepository {

	private EntityManager em;
	public CategoryRepositoryImp(EntityManager em) {
		super(em, Category.class);
		this.em = em;
	}
	

	@Override
	protected String getTableName() {
		return "Category";
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Category> searchByBook(Book book) {
		Query query = em.createQuery("category.searchByBook", Category.class);
		query.setParameter("book", book);
		return (List<Category>) query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Category> searchByKeyword(String keyword) {
		Query query = em.createQuery("categiry.searchByKeyword", Category.class);
		query.setParameter("keyword", keyword);
		return (List<Category>)query.getResultList();
	}


}

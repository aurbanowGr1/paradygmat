package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.Book;
import domain.Tag;
import service.repositories.BaseRepository;
import service.repositories.TagRepository;

public class TagRepositoryImp extends BaseRepository<Tag> implements TagRepository {

	private EntityManager em;
	public TagRepositoryImp(EntityManager em) {
		super(em, Tag.class);
		this.em = em;
	}
	
	@Override
	protected String getTableName() {
		return "Tag";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> searchByBook(Book book) {
		Query query = em.createQuery("tag.searchByBook", Tag.class);
		query.setParameter("book", book);
		return (List<Tag>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> searchByKeyword(String keyword) {
		Query query = em.createQuery("tag.searchByKeyword", Tag.class);
		query.setParameter("keyword", keyword);
		return (List<Tag>) query.getResultList();
	}


}

package service.repositories.imp;

import javax.persistence.EntityManager;
import javax.persistence.Query;


import domain.Reader;
import domain.User;
import service.repositories.BaseRepository;
import service.repositories.UserRepository;

public class UserRepositoryImp extends BaseRepository<User> implements UserRepository {

	private EntityManager em;
	public UserRepositoryImp(EntityManager em) {
		super(em, User.class);
		this.em = em;
	}
	
	@Override
	protected String getTableName() {
		return "User";
	}

	@Override
	public User searchByLogin(String login) {
		Query query = em.createQuery("user.searchByLogin", User.class);
		query.setParameter("login", login);
		return (User) query.getSingleResult();
	}

	@Override
	public User searchByReader(Reader reader) {
		Query query = em.createQuery("user.searchByReader", User.class);
		query.setParameter("reader", reader);
		return (User) query.getSingleResult();
	}
}

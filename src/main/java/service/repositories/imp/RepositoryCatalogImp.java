package service.repositories.imp;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ejb.Stateless;

import service.repositories.*;

@Stateless
public class RepositoryCatalogImp implements RepositoryCatalog {
	@Inject
	private EntityManager em;


	@Override
	public AddressRepository getAddressRepo() {
		return new AddressRepositoryImp(em);
	}
	@Override
	public CopyRepository getCopyRepo() {
		return new CopyRepositoryImp(em);
	}
	@Override
	public BookRepository getBookRepo() {
		return new BookRepositoryImp(em);
	}
	@Override
	public HistoryRepository getHistoryRepo() {
		return new HistoryRepositoryImp(em);
	}
	@Override
	public NotificationRepository getNotificationRepo() {
		return new NotificationRepositoryImp(em);
	}
	@Override
	public RequestRepository getRequestRepo() {
		return new RequestRepositoryImp(em);
	}
	@Override
	public ReaderRepository getReaderRepo() {
		return new ReaderRepositoryImp(em);
	}
	@Override
	public UserRepository getUserRepo() {
		return new UserRepositoryImp(em);
	}
	@Override
	public TagRepository getTagRepo() {
		return new TagRepositoryImp(em);
	}
	@Override
	public CategoryRepository getCategoryRepo() {
		return new CategoryRepositoryImp(em);
	}
}

package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.Copy;
import domain.Copy.Status;
import service.repositories.BaseRepository;
import service.repositories.CopyRepository;

public class CopyRepositoryImp extends BaseRepository<Copy> implements CopyRepository {
	
	private EntityManager em;
	public CopyRepositoryImp(EntityManager em) {
		super(em, Copy.class);
		this.em = em;
	}

	@Override
	protected String getTableName() {
		return "Copy";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Copy> searchByReleaseNumber(int number) {
		Query query = em.createQuery("copy.searchByBook", Copy.class);
		query.setParameter("releaseNumber", number);
		return (List<Copy>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Copy> searchByReleaseDate(long date) {
		Query query = em.createQuery("copy.searchByReleaseDate", Copy.class);
		query.setParameter("releaseDate", date);
		return (List<Copy>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Copy> searchByStatus(Status status) {
		Query query = em.createNamedQuery("copy.searchByReleaseDate", Copy.class);
		query.setParameter("status", status);
		return (List<Copy>) query.getResultList();
	}

}

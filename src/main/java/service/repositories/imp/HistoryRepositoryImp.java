package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.Copy;
import domain.History;
import domain.Reader;
import service.repositories.BaseRepository;
import service.repositories.HistoryRepository;

public class HistoryRepositoryImp extends BaseRepository<History> implements HistoryRepository {

	private EntityManager em;
	public HistoryRepositoryImp(EntityManager em) {
		super(em, History.class);
		this.em = em;
	}
	
	@Override
	protected String getTableName() {
		return "History";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<History> searchByBorrowDate(long borrowDate) {
		Query query = em.createQuery("copy.searchByBook", Copy.class);
		query.setParameter("borrowDate", borrowDate);
		return (List<History>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<History> searchByReturnDate(long returnDate) {
		Query query = em.createNamedQuery("copy.searchByReturnDate", Copy.class);
		query.setParameter("returnDate", returnDate);
		return (List<History>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<History> searchByCopy(Copy copy) {
		Query query = em.createQuery("copy.searchByCopy", Copy.class);
		query.setParameter("copy", copy);
		return (List<History>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<History> searchByReader(Reader reader) {
		Query query = em.createQuery("copy.searchByReader", Copy.class);
		query.setParameter("reader", reader);
		return (List<History>)query.getResultList();
	}

}

package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.Reader;
import domain.Reader.Gender;
import service.repositories.BaseRepository;
import service.repositories.ReaderRepository;

public class ReaderRepositoryImp extends BaseRepository<Reader> implements ReaderRepository {

	private EntityManager em;
	public ReaderRepositoryImp(EntityManager em) {
		super(em, Reader.class);
		this.em = em;
	}
	@Override
	protected String getTableName() {
		return "Reader";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Reader> searchByFirstName(String firstName) {
		Query query = em.createNamedQuery("reader.searchByFirstName", Reader.class);
		query.setParameter("firstName", firstName);
		return (List<Reader>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Reader> searchBySurname(String surname) {
		Query query = em.createNamedQuery("reader.searchBySurname", Reader.class);
		query.setParameter("surname", surname);
		return (List<Reader>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Reader> searchByCity(String city) {
		Query query = em.createNamedQuery("reader.searchByCity", Reader.class);
		query.setParameter("city", city);
		return (List<Reader>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Reader> searchByGender(Gender gender) {
		Query query = em.createNamedQuery("reader.searchByGender", Reader.class);
		query.setParameter("gender", gender);
		return (List<Reader>) query.getResultList();
	}
	
	@Override
	public Reader searchByPesel(String pesel) {
		Query query = em.createQuery("reader.searchByPesel", Reader.class);
		query.setParameter("pesel", pesel);
		return (Reader) query.getSingleResult();
	}
	
	@Override
	public Reader searchByEmail(String email) {
		Query query = em.createQuery("reader.searchByEmail", Reader.class);
		query.setParameter("email", email);
		return (Reader) query.getSingleResult();
	}
	
	@Override
	public Reader searchByPhoneNumber(String phoneNumber) {
		Query query = em.createQuery("reader.searchByPhoneNumber", Reader.class);
		query.setParameter("phoneNumber", phoneNumber);
		return (Reader) query.getSingleResult();
	}


}

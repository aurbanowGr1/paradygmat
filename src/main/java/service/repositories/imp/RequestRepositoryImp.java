package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.*;
import service.repositories.BaseRepository;
import service.repositories.RequestRepository;

public class RequestRepositoryImp extends BaseRepository<Request> implements RequestRepository {
	
	private EntityManager em;
	public RequestRepositoryImp(EntityManager em) {
		super(em, Request.class);
		this.em = em;
	}
	
	@Override
	protected String getTableName() {
		return "Request";
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Request> searchByRequestDate(long requestDate) {
		Query query = em.createQuery("request.searchByRequestDate", Request.class);
		query.setParameter("requestDate", requestDate);
		return (List<Request>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Request> searchByExecutionDate(long executionDate) {
		Query query = em.createQuery("request.searchByExecutionDate", Request.class);
		query.setParameter("executionDate", executionDate);
		return (List<Request>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Request> searchByBook(Book book) {
		Query query = em.createQuery("request.searchByBook", Request.class);
		query.setParameter("book", book);
		return (List<Request>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Request> searchByReader(Reader reader) {
		Query query = em.createQuery("request.searchByReader", Request.class);
		query.setParameter("reader", reader);
		return (List<Request>) query.getResultList();
	}

}

package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import domain.Book;
import service.repositories.BaseRepository;
import service.repositories.BookRepository;

public class BookRepositoryImp extends BaseRepository<Book> implements BookRepository {
	
	private EntityManager em;
	public BookRepositoryImp(EntityManager em) {
		super(em, Book.class);
		this.em = em;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Book> searchByTitle(String title) {
		Query query = em.createQuery("book.searchByTitle", Book.class);
		query.setParameter("title", title);
		return (List<Book>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> searchByAuthorName(String name) {
		Query query = em.createQuery("book.searchByAuthorname", Book.class);
		query.setParameter("authorName", name);
		return (List<Book>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> searchByAuthorSurname(String surname) {
		Query query = em.createQuery("book.searchByAuthorSurname", Book.class);
		query.setParameter("authorSurname", surname);
		return (List<Book>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Book> searchByKind(String kind) {
		Query query = em.createQuery("book.searchByKind", Book.class);
		query.setParameter("kind", kind);
		return (List<Book>) query.getResultList();
	}
	
	@Override
	public List<Book> searchByTags(List<String> tags) {
		//TODO
		return null;
	}

	@Override
	protected String getTableName() {
		return "Book";
	}


}

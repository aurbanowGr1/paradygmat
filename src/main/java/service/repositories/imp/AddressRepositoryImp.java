package service.repositories.imp;

import javax.persistence.EntityManager;

import domain.Address;
import service.repositories.AddressRepository;
import service.repositories.BaseRepository;

public class AddressRepositoryImp extends BaseRepository<Address> implements AddressRepository {

	public AddressRepositoryImp(EntityManager em) {
		super(em, Address.class);
	}

	@Override
	protected String getTableName() {
		return "Address";
	}

}

package service.repositories.imp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import domain.*;
import service.repositories.BaseRepository;
import service.repositories.NotificationRepository;

public class NotificationRepositoryImp extends BaseRepository<Notification> implements NotificationRepository {

	private EntityManager em;
	public NotificationRepositoryImp(EntityManager em) {
		super(em, Notification.class);
		this.em = em;
	}
	

	@Override
	protected String getTableName() {
		return "Notification";
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> searchByReader(Reader reader) {
		Query query = em.createQuery("notification.searchByReader", Notification.class);
		query.setParameter("reader", reader);
		return (List<Notification>)query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> searchByCopy(Copy copy) {
		Query query = em.createNamedQuery("notification.searchByCopy", Notification.class);
		query.setParameter("copy", copy);
		return (List<Notification>)query.getResultList();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> searchByDate(long date) {
		Query query = em.createNamedQuery("notification.searchByDate", Notification.class);
		query.setParameter("date", date);
		return (List<Notification>)query.getResultList();
	}
}

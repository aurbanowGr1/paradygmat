package service.repositories;

public interface RepositoryCatalog {
	
		public AddressRepository getAddressRepo();
		public CopyRepository getCopyRepo();
		public BookRepository getBookRepo();
		public HistoryRepository getHistoryRepo();
		public NotificationRepository getNotificationRepo();
		public RequestRepository getRequestRepo();
		public ReaderRepository getReaderRepo();
		public UserRepository getUserRepo();
		public TagRepository getTagRepo();
		public CategoryRepository getCategoryRepo();
}
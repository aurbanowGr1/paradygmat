package service.repositories;

import java.util.List;

import domain.Copy;
import domain.History;
import domain.Reader;

public interface HistoryRepository extends Repository<History>{

	List<History> searchByBorrowDate(long borrowDate);
	List<History> searchByReturnDate(long returnDate);
	List<History> searchByCopy(Copy copy);
	List<History> searchByReader(Reader reader);
}

package service.repositories;

import java.util.List;

import domain.*;

public interface RequestRepository extends Repository<Request>{
	
	List<Request> searchByRequestDate(long requestDate);
	List<Request> searchByExecutionDate(long executionDate);
	List<Request> searchByBook(Book book);
	List<Request> searchByReader(Reader reader);
}

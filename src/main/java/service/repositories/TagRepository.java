package service.repositories;

import java.util.List;

import domain.Book;
import domain.Tag;

public interface TagRepository extends Repository<Tag> {
	List<Tag> searchByBook(Book book);
	List<Tag> searchByKeyword(String keyword);
	
}
